package com.example.reactivexkotlin

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.reactivexkotlin.databinding.ActivityHomeBinding
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.kotlin.toObservable
import java.util.concurrent.TimeUnit

class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding
    private var result: TextView? = null
    private val list = listOf("one", "two", "three", "four", "five")
    private val listToObservable: Observable<String> = list.toObservable()


    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityHomeBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initViews()
    }

    private fun show(string: String) {
        val prev = result!!.text.toString()
        result!!.text = prev.plus(string)
    }

    private val observer: Observer<String> = object : Observer<String> {
        override fun onSubscribe(d: Disposable) {
            show("New Subscription\n")
        }

        override fun onNext(t: String) {
            show(t.plus("\n"))
        }

        override fun onError(e: Throwable) {
            e.printStackTrace()
        }

        override fun onComplete() {
            show("Completed!")
        }
    }

    private val observableUsingCreate = Observable.create<String> {
        it.onNext("Emit 1")
        it.onNext("Emit 2")
        it.onNext("Emit 3")
        it.onNext("Emit 4")
        it.onNext("Emit 5")
    }

    private val intervalObserver = object: Observer<Long>{
        private lateinit var disposable: Disposable
        override fun onSubscribe(d: Disposable) {
            this.disposable = d
            show("New Interval Subscription\n")
        }

        override fun onNext(t: Long) {
            if(t>=10){
                disposable.dispose()
            }
            show("Received: ".plus(t.toString()).plus("\n"))
        }

        override fun onError(e: Throwable) {
            e.printStackTrace()
        }

        override fun onComplete() {
            show("Completed")
        }
    }

    private val observableUsingIterable: Observable<String> = Observable.fromIterable(list)
    private val observableUsingJust: Observable<String> = Observable.just("SampleString")
    private val observableUsingInterval: Observable<Long> = Observable.interval(10, TimeUnit.MILLISECONDS)

    private fun initViews() {
        result = binding.resultTV

        listToObservable.subscribe(observer)
        observableUsingCreate.subscribe(observer)
        observableUsingIterable.subscribe(observer)
        observableUsingJust.subscribe(observer)
        observableUsingInterval.subscribe(intervalObserver)
    }
}