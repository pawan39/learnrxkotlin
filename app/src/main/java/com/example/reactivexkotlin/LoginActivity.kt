package com.example.reactivexkotlin

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.reactivexkotlin.databinding.ActivityLoginBinding
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding

    private lateinit var email: TextInputEditText
    private lateinit var password: TextInputEditText
    private lateinit var progressBar: ProgressBar
    private lateinit var loginBtn: MaterialButton

    //    Observables for observing state of login form
    private lateinit var validationObservable: Observable<Boolean>
    private lateinit var emailObservable: Observable<String>
    private lateinit var passwordObservable: Observable<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViews()
    }

    private fun initViews() {
        email = binding.loginEmailEt
        password = binding.loginPasswordEt
        loginBtn = binding.loginBtn
        progressBar = binding.loginProgressBar
    }

    override fun onStart() {
        super.onStart()
        setActions()
    }

    private fun setActions() {
//        Observe provided email address
        emailObservable = RxTextView.textChanges(email)
            .skip(1)
            .map { it.toString() }

//        Observe provided password
        passwordObservable = RxTextView.textChanges(password)
            .skip(1)
            .map { it.toString() }

//        Create new Observable from email and password Observables
        validationObservable = Observable.combineLatest(
            emailObservable, passwordObservable
        ) { email, password -> isValidForm(email, password) }

//        Set loginButton active on inactive based on provided email and password(Just validation only.)
        validationObservable.subscribe(object : DisposableObserver<Boolean>() {
            override fun onNext(value: Boolean?) {
                changeLoginButtonState(value!!)
            }

            override fun onError(e: Throwable?) {
            }

            override fun onComplete() {
            }
        })

//        Set Errors on email and password edittext
        emailObservable.subscribe(object : DisposableObserver<String>() {
            override fun onNext(value: String?) {
                if (value != null && isValidEmail(value)) {
                    email.error = null
                } else email.error = "Invalid Email address"
            }

            override fun onError(e: Throwable?) {}

            override fun onComplete() {}
        })

        passwordObservable.subscribe(object : DisposableObserver<String>() {
            override fun onNext(value: String?) {
                if (value != null && isValidPassword(value)) {
                    password.error = null
                } else {
                    password.error = "Password should be of 5 to 10 characters long."
                }
            }

            override fun onError(e: Throwable?) {}

            override fun onComplete() {}
        })
    }

    private fun changeLoginButtonState(result: Boolean) {
        this.loginBtn.isEnabled = result
    }

    private fun isValidEmail(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isValidPassword(password: String): Boolean {
        return password.length in 5..10
    }

    private fun isValidForm(emailData: String, passwordData: String): Boolean {
        return isValidEmail(emailData) && isValidPassword(passwordData)
    }

    fun login(view: View) {
        Toast.makeText(this, "Welcome" + email.text.toString(), Toast.LENGTH_SHORT).show()
        startActivity(Intent(this, HomeActivity::class.java))
        this.finish()
    }
}