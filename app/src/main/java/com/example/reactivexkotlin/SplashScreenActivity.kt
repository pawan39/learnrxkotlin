package com.example.reactivexkotlin

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.reactivexkotlin.databinding.ActivitySplashScreenBinding
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class SplashScreenActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()
        Executors.newSingleThreadScheduledExecutor().schedule(
            {
                startActivity(Intent(this, LoginActivity::class.java))
                this.finish()
            },
            2,
            TimeUnit.SECONDS
        )
    }
}